<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class v_event_peserta extends Model
{

    protected $table = 'v_event_peserta';
    protected $primaryKey = 'id_event_peserta';
}
