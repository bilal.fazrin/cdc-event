<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class t_event extends Model
{

    protected $table = 't_event';
    protected $primaryKey = 'id_event';
    protected $fillable = [
        'id_event','poster','nama_event', 'type_event', 'awal_pendaftaran', 'akhir_pendaftaran', 'tanggal_acara','headline'
    ];
}
