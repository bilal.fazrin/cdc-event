<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class t_event_peserta extends Model
{

    protected $table = 't_event_peserta';
    protected $primaryKey = 'id_event_peserta';
    protected $fillable = [
        'id_event_peserta','no_tiket','id_event', 'id_pengenal', 'awal_pendaftaran','status_kehadiran'
    ];
}
