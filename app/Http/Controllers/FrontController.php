<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\t_event;
use DB;

class FrontController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
      $t_event = DB::table('t_event')->get();
    	return view('welcome',compact('t_event'));
  }
}
