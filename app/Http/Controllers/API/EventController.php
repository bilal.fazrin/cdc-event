<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\t_event;
use DB;
use App\t_event_peserta;
use App\v_event_peserta;

use DateTime;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return t_event::latest()->paginate(5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $t_event = new t_event;
        $t_event->nama_event = $request['nama_event'];
        $t_event->type_event = $request['type_event'];
        $t_event->awal_pendaftaran = $request['awal_pendaftaran'];
        $t_event->akhir_pendaftaran = $request['akhir_pendaftaran'];
        $t_event->tanggal_acara = $request['tanggal_acara'];
        $t_event->headline = $request['headline'];
        if($request->poster){
          $name = time().'.' . explode('/', explode(':', substr($request->poster, 0, strpos($request->poster, ';')))[1])[1];
          \Image::make($request->poster)->save('img/event/'.$name);
          $t_event->poster = $name;
        }
        $t_event->save();

    }

    /*public function storePeserta(Request $request)
    {
        $id_pengenal = Auth::user()->id_pengenal;
        $is_exist = DB::table('t_event_peserta')->where([
            ['ID_EVENT', '=', $request['ID_EVENT']],
            ['ID_PENGENAL', '=', $id_pengenal],
        ])->get();
        $t_event_peserta = new t_event_peserta;
        $t_event_peserta->ID_EVENT = $request['ID_EVENT'];
        $t_event_peserta->ID_PENGENAL = $id_pengenal;
        if(count($is_exist)==1)
          {
            echo 'aa';
          }
        else {
          $t_event_peserta->save();
        }
    } */

    public function storePeserta(Request $request){
      $t_event_peserta = new t_event_peserta;
      $id_pengenal = Auth::user()->id_pengenal;
      $registered = 0;
      $is_exist = DB::table('t_event_peserta')->where([
          ['id_event', '=', $request['id_event']],
          ['id_pengenal', '=', $id_pengenal],
      ])->get();
      if(count($is_exist)==1)
        {
          $registered = 1;
          return $registered;
        }
      else {
        $t_event_peserta->id_event = $request['id_event'];
        $t_event_peserta->id_pengenal = $id_pengenal;

        do {
           $refrence_id = mt_rand( 100000000, 999999900 );
        } while ( DB::table( 't_event_peserta' )->where( 'no_tiket', $refrence_id )->exists() );


        $t_event_peserta->no_tiket = $refrence_id;
        $t_event_peserta->save();
        return $registered;
      }
    }

    public function checkExisting(){

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return t_event::where('id_event',$id)->first();
    }

    public function search()
    {
        //
        if($search = \Request::get('q')){
          $t_event = t_event::where(function($query) use ($search){
              $query->where('nama_event','LIKE',"%$search%")
                    ->orWhere('type_event','LIKE',"%$search%")
                    ->orWhere('headline','LIKE',"%$search%");
          })->paginate(5);
        }
        return $t_event;
    }

    public function searchPeserta()
    {
        //
        if($search = \Request::get('q')){
          $v_event_peserta = v_event_peserta::where(function($query) use ($search){
              $query->where('id_pengenal','LIKE',"%$search%")
                    ->orWhere('nama_peserta','LIKE',"%$search%")
                    ->orWhere('nama_event','LIKE',"%$search%")
                    ->orWhere('id_event_peserta','LIKE',"%$search%");
          })->paginate(5);
        }
        return $v_event_peserta;
    }

    public function validEvent(){
        $now = new DateTime();
        return t_event::latest()->paginate(5);
        /*return t_event::where('awal_pendaftaran','>=',$now)
                        ->orWhere('akhir_pendaftaran','<=',$now)->paginate(5);*/

    }

    public function historyEvent(){
        return v_event_peserta::paginate(5);
    }
    public function historyEventPeserta(){
        $user = Auth::user()->id_pengenal;
        return v_event_peserta::where('id_pengenal',$user)->paginate(5);
    }
    public function tiket($no_tiket){
        return v_event_peserta::where('id_event_peserta',$no_tiket)->first();
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //$t_event = new t_event;
      $t_event = t_event::findOrFail($id);
      $currentPhoto = $t_event->poster;
      if($request->poster != $currentPhoto){
        $name = time().'.' . explode('/', explode(':', substr($request->poster, 0, strpos($request->poster, ';')))[1])[1];

        \Image::make($request->poster)->save('img/event/'.$name);

        $request->merge(['poster'=> $name]);
      }
      $t_event->update($request->all());
      return ['message' => 'Updated the user info'];
    }

    public function absenPeserta($id)
    {
      $t_event_peserta = t_event_peserta::findOrFail($id);
      $t_event_peserta->update(['status_kehadiran'=>'HADIR']);
      $t_event_peserta->update(['updated_at']);
      return ['message' => 'Updated the user info'];
    }

    public function absensiPeserta($no_tiket)
    {
      return v_event_peserta::where('id_event_peserta',$no_tiket)->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $t_event = t_event::findOrFail($id);

      $t_event->delete();

      return ['message' => 'User Deleted'];
    }
}
