<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\t_event_peserta;
use App\User;

class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::latest()->paginate(100);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this -> validate($request,[
          'id_pengenal'  =>  'required|integer|unique:users',
          'name'  =>  'required|string|max:191',
          'email'  =>  'required|string|email|max:191|unique:users',
          'password'  =>  'required|string|min:6'
        ]);

        return User::create([
          'id_pengenal' => $request['id_pengenal'],
          'name' => $request['name'],
          'universitas' => $request['universitas'],
          'status_mahasiswa' => $request['status_mahasiswa'],
          'email' => $request['email'],
          'type' => $request['type'],
          'password' => Hash::make($request['password']),
        ]);
    }

    public function regisOnTheSpot(Request $request)
    {
        $this -> validate($request,[
          'id_pengenal'  =>  'required|integer|unique:users',
          'name'  =>  'required|string|max:191',
          'email'  =>  'required|string|email|max:191|unique:users',
          'password'  =>  'required|string|min:6'
        ]);

        $users = new User;
        $users->id_pengenal = $request['id_pengenal'];
        $users->name = $request['name'];
        $users->universitas = $request['universitas'];
        $users->status_mahasiswa = $request['status_mahasiswa'];
        $users->email = $request['email'];
        $users->type = 'user';
        $users->password = Hash::make($request['password']);
        $users->save();

        $t_event_peserta = new t_event_peserta;
        $t_event_peserta->id_event = 2;
        $t_event_peserta->id_pengenal = $users->id_pengenal;
        $t_event_peserta->status_kehadiran = 'HADIR';
        
        $t_event_peserta->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function current()
    {
        $user = Auth::user()->id_pengenal;
        return User::where('id_pengenal',$user)->first();
    }

    public function search()
    {
        //
        if($search = \Request::get('q')){
          $users = User::where(function($query) use ($search){
              $query->where('name','LIKE',"%$search%")
                    ->orWhere('email','LIKE',"%$search%")
                    ->orWhere('email','LIKE',"%$search%");
          })->paginate(5);
        }
        return $users;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this -> validate($request,[
          'name'  =>  'required|string|max:191',
          'email'  =>  'required|string|email|max:191|unique:users,email,'.$user->id,
          'password'  =>  'sometimes|min:6'
        ]);

        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $user->update($request->all());
        return ['message' => 'Updated the user info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);

        $user->delete();

        return ['message' => 'User Deleted'];
    }
}
