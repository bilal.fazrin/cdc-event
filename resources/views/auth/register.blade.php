@extends('layouts.app')


@section('content')
<div class="container" style="background-image:url('/img/landing.png'); background-size: 100% 100%;">
  <div class="row">
    <div class="col">
      <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
      <br><br><br><br><br><br><br><br><br><br><br>
    </div>
    <div class="col">
      <br><br><br>
      <div class="card-body-text-center">
              <div class="row mt-5 card-centered">
                <div class="card-body">
                    <div class="text-center"><b><h1>{{ __('REGISTER') }}</h1></b></div>
                    <div class="row mt-5"></div>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="id_pengenal" class="col-md-4 col-form-label text-md-right">{{ __('NIM / No KTP') }}</label>

                            <div class="col-md-6">
                                <input id="id_pengenal" type="number" class="form-control{{ $errors->has('id_pengenal') ? ' is-invalid' : '' }}" name="id_pengenal" value="{{ old('id_pengenal') }}" required autofocus>
                                @if ($errors->has('id_pengenal'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('id_pengenal') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="universitas" class="col-md-4 col-form-label text-md-right">{{ __('Universitas') }}</label>

                            <div class="col-md-6">
                                <input id="universitas" type="text" class="form-control{{ $errors->has('universitas') ? ' is-invalid' : '' }}" name="universitas" value="{{ old('universitas') }}" required autofocus>

                                @if ($errors->has('universitas'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('universitas') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status_mahasiswa" class="col-md-4 col-form-label text-md-right">{{ __('Status Mahasiswa') }}</label>

                            <div class="col-md-6">
                                <select id="status_mahasiswa"  class="form-control{{ $errors->has('status_mahasiswa') ? ' is-invalid' : '' }}" name="status_mahasiswa" value="{{ old('status_mahasiswa') }}" required autofocus>
                                  <option value="alumni">Alumni</option>
                                  <option value="mahasiswa">Mahasiswa</option>
                                </select>
                                @if ($errors->has('status_mahasiswa'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('status_mahasiswa') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
              </div>

      </div>
    </div>
  </div>
</div>
@endsection
