<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="assets/img/favicon.png">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Tel-U Career Event</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="bootstrap3/css/bootstrap.css" rel="stylesheet" />
	<link href="assets/css/gsdk.css" rel="stylesheet" />
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--     Font Awesome     -->
    <link href="bootstrap3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>

</head>

<body>
<!-- Nav start -->
<div id="navbar-full">
    <div id="navbar">
    <!--
        navbar-default can be changed with navbar-ct-blue navbar-ct-azzure navbar-ct-red navbar-ct-green navbar-ct-orange
        -->
        <nav class="navbar navbar-ct-red navbar-fixed-top navbar-transparent" role="navigation">
          <div class="alert alert-success hidden">
            <div class="container">
                <b>Lorem ipsum</b> dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
            </div>
          </div>

          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="#">
                        <div class="navbar-brand">
                            <img src="assets/img/logo.png" height="25px" width="80px">
                        </div>
                </a>
                <a class="navbar-brand" href="#"></i>Tel-U Career Event Portal </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

               <form class="navbar-form navbar-left navbar-search-form" role="search">
                 <div class="form-group">
                      <input type="text" value="" class="form-control" placeholder="Search...">
                 </div>
              </form>
              <ul class="nav navbar-nav navbar-right">
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Logged in as {{ Auth::user()->name }} <b class="caret"></b></a>
                                  <ul class="dropdown-menu">
                                    <li><a href="{{ route('login') }}">Backend Menu</a></li>
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                  document.getElementById('logout-form').submit();">
                                     Logout</a>
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         @csrf
                                     </form></li>
                                  </ul>
                      </li>

                @endguest
              </ul>

            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <div class="blurred-container">
            <div class="img-src" style="background-image: url('assets/img/bg.jpeg')"></div>
        </div>
    </div><!--  end navbar -->

</div>
<!-- nav end-->
<div class="main">
  <div class="section">
           <div class="container">
						 <h2 class="section-title">Events</h2>
						 <hr>
						 <div class="row">
							<?php $count=0; ?>
						  @foreach ($t_event as $event)
							<vl>
							<div class="col-md-3" style="height:500px">
									<div class="card card-product card-plain">
											<div class="image">
													<a data-toggle="modal" data-target="#myModal">
															<img src="img/event/{{$event->poster}}" alt="..." class="card" height="300px">
													</a>
											</div>
											<div class="card-footer">
													<a href="http://localhost:8000/listEvent">
															<h4 class="title">{{$event->nama_event}}</h4>
													</a>
													<button href="#" class="btn btn-block btn-lg btn-danger">Register</button>
											</div>
									</div> <!-- end card -->
							 </div>
							 <?php
							 $count++;

							 if(($count%4)==0){
							 ?>
						 		</div>
								<hr>
								<div class="row">
								<?php };?>
						  @endforeach
							</div>




           </div>
    </div>



    <!--<div class="container tim-container" style="max-width:800px; padding-top:100px">
       <h1 class="text-center">Awesome looking header <br> just for my friends<small class="subtitle">This is like a small motto before the story.</small></h1>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
       <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    </div>
    <div class="space"></div>-->
<!-- end container -->
        <!-- Modal --> <!--
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              </div>
              <div class="modal-body">
                ...
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
                <div class="divider"></div>
                <button type="button" class="btn btn-info btn-simple">Save</button>
              </div>
            </div>
          </div>
        </div>
-->


 <!--    end modal -->



<!-- CAROUSEL -->


<!-- end Carousel -->

</div>



</body>
<div class="main social-line" style="background-color:rgba(255, 76, 64, 0.98);">
<br>
<br>
</div>
<div class="main social-line">
    <div class="container">

        <div class="row">
            <div class="col-md-4 text-center"><h4 class="sharing-title">Social Media : </h4></div>
            <div class="col-md-2 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                <button id="twitter" class="btn btn-info  btn-round btn-wd btn-block btn-fll social-share social-twitter sharrre"><i class="fa fa-twitter"></i> Tweet </button>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                <button id="google" class="btn btn-danger btn-round btn-block  btn-wd btn-fll social-share social-google-plus sharrre"><i class="fa fa-instagram"></i> Share</button>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
              <button id="facebook" class="btn btn-primary btn-round btn-wd  btn-block btn-fil social-share sharrre"><i class="fa fa-facebook-square"></i> Share</button>
            </div>
            <div class="col-md-2 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
              <button id="linkedin" class="btn btn-primary btn-round btn-wd  btn-block btn-fil social-share sharrre"><i class="fa fa-linkedin-square"></i> Share</button>
            </div>
        </div>
    </div>


</div>
<div class="main social-line" style="background-color:rgba(255, 76, 64, 0.98);">
	<br>
	<br>
</div>


<!-- start footer -->
<footer class="footer footer-big" style="background-image: url('assets/img/maroon.png');">
            <!-- .footer-black is another class for the footer, for the transparent version, we recommend you to change the url of the image with your favourite image.          -->
<br>
            <div class="row">

                    <div class="col-md-5 col-md-offset-1">
                        <h4 class="title">Contact Us</h4>
                        <nav>
                          <p>Tel-U Career Secretariat <br>
  Bangkit Building 3rd Floor Telkom University Bandung Technoplex<br>
  Jl. Telekomunikasi Terusan Buah Batu Bandung<br>
  Email : cdc@telkomuniversity.ac.id <br>
  Phone : 022-7564108 ext. 2222<br>
  &nbsp;</p>
                        </nav>
                    </div>
                    <div class="col-md-5 col-md-offset-1">
                        <h4 class="title"> Help and Support</h4>
                        <nav>
                            <ul>
                                <li>
                                    <a href="#">
                                       Contact Us
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                       How it works
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Terms &amp; Conditions
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Company Policy
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                       Money Back
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>

        <div class="overlayer">
          <div class="container">
              <div class="row">
                  <div class="credits">
                      <h6>© 2019 CDC TEL-U</h6>
                  </div>
                </div>
          </div>
        </div>
        </footer>

<!-- end footer -->

    <script src="jquery/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

	<script src="bootstrap3/js/bootstrap.js" type="text/javascript"></script>
	<script src="assets/js/gsdk-checkbox.js"></script>
	<script src="assets/js/gsdk-radio.js"></script>
	<script src="assets/js/gsdk-bootstrapswitch.js"></script>
	<script src="assets/js/get-shit-done.js"></script>

    <script src="assets/js/custom.js"></script>
</html>
