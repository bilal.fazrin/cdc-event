@extends('welcome')

@section('event')
<div class="section">
         <div class="container">
             <h2 class="section-title">Events</h2>
             <div class="row">
                  <div class="col-md-4">
                      <div class="card card-product card-plain">
                          <div class="image">
                              <a data-toggle="modal" data-target="#myModal">
                                  <img src="assets/img/poster.png" alt="..." class="card">
                              </a>
                          </div>
                          <div class="content">
                              <a href="#">
                                  <h4 class="title">The 15th Career Day of Telkom University</h4>
                              </a>
                              <button href="#fakelink" class="btn btn-block btn-lg btn-danger">Register</button>
                          </div>
                      </div> <!-- end card -->
                   </div>
                    <div class="col-md-4">
                      <div class="card card-product card-plain">
                          <div class="image">
                              <a href="#">
                                  <img src="assets/img/tokped.jpeg" alt="..." class="card">
                              </a>
                          </div>
                          <div class="content">
                              <a href="#">
                                  <h4 class="title">Tokopedia TechaBreak</h4>
                              </a>
                              <button href="#fakelink" class="btn btn-block btn-lg btn-danger">Register</button>
                          </div>
                      </div> <!-- end card -->
                   </div>
                    <div class="col-md-4">
                      <div class="card card-product card-plain">
                          <div class="image">
                              <a href="#">
                                  <img src="assets/img/huawei.jpeg" alt="..." class="card">
                              </a>
                          </div>
                          <div class="content">
                              <a href="#">
                                  <h4 class="title">Huawei Connected Campus Recruitment 2019</h4>
                              </a>
                              <button href="#fakelink" class="btn btn-block btn-lg btn-danger">Register</button>
                          </div>
                      </div> <!-- end card -->
                   </div>
             </div>
             <div class="row">
                  <div class="col-md-4">
                      <div class="card card-product card-plain">
                          <div class="image">
                              <a href="#">
                                  <img src="assets/img/poster.png" alt="..." class="card">
                              </a>
                          </div>
                          <div class="content">
                              <a href="#">
                                  <h4 class="title">The 15th Career Day of Telkom University</h4>
                              </a>
                              <button href="#fakelink" class="btn btn-block btn-lg btn-danger">Register</button>
                          </div>
                      </div> <!-- end card -->
                   </div>
                    <div class="col-md-4">
                      <div class="card card-product card-plain">
                          <div class="image">
                              <a href="#">
                                  <img src="assets/img/poster.png" alt="..." class="card">
                              </a>
                          </div>
                          <div class="content">
                              <a href="#">
                                  <h4 class="title">The 15th Career Day of Telkom University</h4>
                              </a>
                              <button href="#fakelink" class="btn btn-block btn-lg btn-danger">Register</button>
                          </div>
                      </div> <!-- end card -->
                   </div>
                    <div class="col-md-4">
                      <div class="card card-product card-plain">
                          <div class="image">
                              <a href="#">
                                  <img src="assets/img/poster.png" alt="..."  class="card">
                              </a>
                          </div>
                          <div class="content">
                              <a href="#">
                                  <h4 class="title">The 15th Career Day of Telkom University</h4>
                              </a>
                              <button href="#fakelink" class="btn btn-block btn-lg btn-danger">Register</button>
                          </div>
                      </div> <!-- end card -->
                   </div>
             </div>
         </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Modal title</h4>
        </div>
        <div class="modal-body">
          ...
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Close</button>
          <div class="divider"></div>
          <button type="button" class="btn btn-info btn-simple">Save</button>
        </div>
      </div>
    </div>
  </div>



<!--    end modal -->
  @endsection
