
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { Form, HasError, AlertError } from 'vform'


import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
  toast: true,
  position: 'top',
  showConfirmButton: true
});

window.toast = toast;


window.form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.component('pagination', require('laravel-vue-pagination'));

import VueRouter from 'vue-router'
Vue.use(VueRouter)

let routes = [
  { path: '/home', component: require('./components/user/Home.vue')},
  { path: '/event', component: require('./components/superadmin/Event.vue')},
  { path: '/absensi/:id', component: require('./components/superadmin/absen.vue')},
  { path: '/tiket/:id', component: require('./components/user/Tiket.vue')},
  { path: '/listEvent', component: require('./components/user/Event.vue')},
  { path: '/listPeserta', component: require('./components/superadmin/listPeserta.vue')},
  { path: '/users', component: require('./components/superadmin/Users.vue')},
  { path: '/regisOnTheSpot', component: require('./components/superadmin/regisOnTheSpot.vue')}
]

const router = new VueRouter({
  mode: 'history',
  routes // short for `routes: routes`
});

Vue.filter('upText',function(text){
  return text.toUpperCase();
});


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    router
});
