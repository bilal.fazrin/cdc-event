<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api')->get('/t_event', function (Request $request) {
    return $request->t_event();
});


Route::apiResources([
  'user'=>'API\UserController',
  't_event'=>'API\EventController',
]);

route::get('findUser','API\UserController@search');
route::get('tiket/{id}','API\EventController@tiket');
route::get('findEvent','API\EventController@search');
route::get('findPeserta','API\EventController@searchPeserta');
route::get('validEvent','API\EventController@validEvent');
route::get('historyEvent','API\EventController@historyEvent');
route::get('historyEventPeserta','API\EventController@historyEventPeserta');
route::post('storePeserta','API\EventController@storePeserta');
route::put('absenPeserta/{id}','API\EventController@absenPeserta');
route::get('absensi/{id}','API\EventController@absensiPeserta');
route::get('current','API\UserController@current');
route::post('regisOnTheSpot','API\UserController@regisOnTheSpot');
