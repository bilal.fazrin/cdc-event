<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TEventPeserta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('t_event_peserta', function (Blueprint $table) {
           $table->increments('id_event_peserta');
           $table->string('id_event');
           $table->string('no_tiket');
           $table->integer('id_pengenal');
           $table->enum('status_kehadiran', ['HADIR', 'TIDAK HADIR'])->default('TIDAK HADIR');
           $table->timestamp('created_at')->nullable();
           $table->timestamp('updated_at')->nullable();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('t_event_peserta');
     }
}
