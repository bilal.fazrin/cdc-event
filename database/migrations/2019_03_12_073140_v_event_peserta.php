<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VEventPeserta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE VIEW v_event_peserta AS
        SELECT bb.no_tiket, aa.id_pengenal id_pengenal,aa.name nama_peserta,bb.id_event_peserta id_event_peserta,
        cc.nama_event nama_event, cc.tanggal_acara tanggal_acara, bb.created_at waktu_register,
        bb.status_kehadiran status_kehadiran, bb.updated_at
        FROM users aa, t_event_peserta bb, t_event cc
        where aa.id_pengenal = bb.id_pengenal and bb.id_event = cc.id_event
        order by id_event_peserta desc ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("v_event_peserta");
    }
}
