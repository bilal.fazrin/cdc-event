<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('t_event', function (Blueprint $table) {
           $table->increments('id_event');
           $table->string('nama_event');
           $table->string('poster')->nullable();
           $table->enum('type_event', ['1','2','3','4']);
           $table->date('awal_pendaftaran');
           $table->date('akhir_pendaftaran');
           $table->date('tanggal_acara');
           $table->mediumText('headline')->nullable();
           $table->timestamp('created_at')->nullable();
           $table->timestamp('updated_at')->nullable();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists('t_tracer');
     }
}
